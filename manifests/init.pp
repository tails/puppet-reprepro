# Install and configure a reprepro debian package archive
#
# @param ensure
#   Set this to 'absent' to uninstall reprepro and remove associated files.
#
class reprepro (
  $ensure = present,
) {
  package {
    'reprepro': ensure => $ensure;
  }

  file { '/usr/local/bin/reprepro-export-key':
    ensure => $ensure,
    source => 'puppet:///modules/reprepro/reprepro-export-key.sh',
    owner  => root,
    group  => root,
    mode   => '0755',
  }
}
