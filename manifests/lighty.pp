# Configure lighttpd to serve files for the archive
#
# XXX This apparently uses code from another module which is not a good
#   practice.
#
class reprepro::lighty inherits lighttpd {
  include reprepro
  file { '/etc/lighttpd/conf-available/20-reprepro.conf':
    ensure  => present,
    content => "alias.url += ( \"/debian/\" => \"${reprepro::basedir}/\" )\n",
  }

  file { '/etc/lighttpd/conf-enabled/20-reprepro.conf':
    ensure  => link,
    target  => '/etc/lighttpd/conf-available/20-reprepro.conf',
    require => File['/etc/lighttpd/conf-available/20-reprepro.conf'],
    notify  => Service['lighttpd'];
  }
}
